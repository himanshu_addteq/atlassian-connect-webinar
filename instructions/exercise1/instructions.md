# Exercise #1

Install the hello world add-on on a local JIRA instance

## Vagrant VMs

These exercises depend on the setup instructions which are described here:

* [Option 1 - Use a Vagrant box to start coding fast](https://bitbucket.org/atlassianlabs/atlassian-connect-webinar/wiki/Using%20a%20Vagrant%20Box.md)
* [Option 2 - setup all the dependancies needed](https://bitbucket.org/atlassianlabs/atlassian-connect-webinar/wiki/Installing%20prerequisites%20on%20your%20machine)
    
Following these instructions mean you will have vagrant running and be
able to ssh to it before this class starts.

Remember to halt your vagrant VMs at the end of the class to free up the
ports it forwards through.

## Building the addon

Open `config.json` and change:

    addonKey (must be globally unique, so use something long and complex)
    addonName (make it unique if possible, e.g. with your initials)
    
Install the dependencies, on the VM:
    
    $ cd /vagrant
    $ npm install
        
Deploy your changes, on the VM:

    $ cd /vagrant/bin/linux
    $ ./build-dev

The files will be copied to `build/dev`, and if you're already running the http server, the changes will now
be available.

*If you make changes to your descriptor you will need to use the UPM to upload your plugin.  If you are just
changing the resource files, then this step is not required.*

### Starting a web server to host the add-on

Start the web server, on the VM:

    $ cd /vagrant/bin/linux
    $ ./start-http-server

YOUR ADD-ON IS RUNNING!

Test it by going to [`http://localhost:8000`](http://localhost:8000).

# Installing your addon to JIRA
   
## Start a local JIRA Instance
   
Start JIRA, on the VM:

    $ cd /vagrant/bin/linux
    $ ./start-jira.sh

JIRA IS NOW RUNNING! (It will be, you just have to wait a bit.)

Test it by going to [`http://localhost:2990/jira`](http://localhost:2990/jira) and login as admin (password: admin).

## Installing an add-on

Use the administration interface on JIRA to install your addon:

    Admin -> Add-ons
    Manage add-ons
    Upload add-on 

Use the descriptor: [`http://localhost:8000/atlassian-connect.json`](http://localhost:8000/atlassian-connect.json)

JIRA should now install your addon!
 
It everything worked, you will see the 'Click Me' item in the top navigation bar (*You will need
to refresh the page after installing the addon*).  This link and  the target page have been defined
in the addon you just wrote.

Awesome!

# Some tips

## Validating a descriptor

If you have problems installing your descriptor, you can use the online
validator here:

[`https://atlassian-connect-validator.herokuapp.com/validate`](https://atlassian-connect-validator.herokuapp.com/validate)
       
## Finding UI extension points
    
A third party plugin exists that will highlight all the UI extension points in JIRA.  It's an easy way to
find the right names for the location of what you want to extend:
    
You can find this plugin here: [`https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder`](https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder)
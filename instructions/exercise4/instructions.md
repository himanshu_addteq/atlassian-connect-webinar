## Exercise #4

Create a dynamic addon using Atlassian Connect Express.

## Create a new ACE project

Add the following as a new line to your local machine's `/etc/hosts`:

    127.0.0.1   precise32
    
Install ACE and create a new project, on the VM:
    
    $ atlas-connect new <project_name>
    $ cd <project_name>
    $ npm install

Start your new ACE based addon by running, on the VM:
    
    $ cd <project_name>
    $ node app.js

Open the ACE documentation and have it available during your development.  It contains a wealth of 
useful information:

    https://bitbucket.org/atlassian/atlassian-connect-express

## Step 1

Update your ACE plugin to to display the users name in the hello-world page.  The current user
name is passed to you as the URL parameter `user_key`.

## Step 2

Update your ACE plugin to to display the current user's full name in the hello-world page.  You will need
to can call the REST resource `/rest/api/latest/myself` to get your full name.

## Step 3

Using a webhook, keep track of issues created by each user.  You will need to listen for the appropriate
webhook and store the record it somewhere so it can be retrieved later.

You can read up on webhooks here:
    
    https://developer.atlassian.com/static/connect/docs/modules/jira/webhook.html

Add the list of issue keys created by the user viewing the the hello-world view.

## Step 4

Deploy the add-on in development mode on Heroku.  Firebase only supports static files so you need to
try out a different hosting service.

## Tips

### Decoding a JWT token

In some cases, you may need to debug the messages going back to your product.  You may want to understand
what the jwt tokens represent, you can do that using this web service:

    http://jwt-decoder.herokuapp.com/jwt/decode

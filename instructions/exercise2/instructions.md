# Exercise #2

Update the "Hello World" message to "Hello <user name>", where the user name is the currently
logged in user.

# Find the REST Resource

## Accessing the REST API Browser (RAB)

To find the username of the current user, you need to call a REST resource from your addon.  To find
an appropriate REST resource, you can use the REST Api Browser.

To access the Rest Api Browser use this URL:

    http://localhost:2990/jira/plugins/servlet/restbrowser
    
Find an appropriate REST resource to find the current user.

## Making a REST call to the Atlassian host using the Javascript API

Connect provides a JavaScript based API for making authenticated calls back to the host product.  This
means you can ignore the low level details of authentication and concentrate on writing an awesome
addon.

Documentation on how to make calls to the host product from your addon:

    https://developer.atlassian.com/static/connect/docs/javascript/module-request.html

Update your plugin to call back in to JIRA to collect the current username.

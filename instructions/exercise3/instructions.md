# Exercise #3

Deploy your addon to marketplace and host it on firebase.

## Deploy your add-on on Firebase

### Create a firebase project

Go to `www.firebase.com` and create an account and create an app through the firebase website.
   
### Configure the build

Open `config.json` and update the baseURL for the add-on in production (it should
be `<your_app_name>.firebaseapp.com`).

### Build the add-on for the target environment

Build the production version of your addon, on the VM:

    $ cd /vagrant/bin/linux
    $ ./build-prod.sh

### Deploy to firebase

Deploy to firebase, on the VM:

    $ cd /vagrant
    $ npm install firebase
    $ firebase init
    $ firebase deploy
    
When asked for the public directory, use: `build/prod`

## Release to marketplace

Atlassian distributes third party plugins through `marketplace.atlassian.com`.  You will register your
addon here as a private addon.  This is a common way for vendors to test their plugin before releasing
to the wild.

### Create Private Addon

Log in to `http://marketplace.atlassian.com` and create a private listing for your addon.  Use the
images from the folder `marketplace_assets`.

Make sure the addon AND the version are PRIVATE!

## Install the add-on on a jira-dev instance

You can now install the addon in your Cloud instance, this time from marketplace instead of your local
instance.

Enable private listings in your local UPM in the Manage Add-ons screen, at the bottom,
`Settings > Enable Private Listings`.

Find the private URL for your addon:

    Navigate to your plugin in market place
    Click `Manage add-on`
    Click `Private Listings`

The installation URL is linked at the top of the private listings page.  Use this URL to install your
plugin on your jira-dev instance.

Private listings can be used by vendors to allow specific customers or test instances to install
their private addons.  This can be used for demonstration, testing or evaluation purposes.
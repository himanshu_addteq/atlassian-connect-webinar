# Atlassian Connect Webinar

## Slides
[View the slides on SpeakerDeck](https://speakerdeck.com/rwhitbeck/atlassian-connect)

## Development Environment 

* [Option 1 - Use a Vagrant box to start coding fast](https://bitbucket.org/atlassianlabs/atlassian-connect-webinar/wiki/Using%20a%20Vagrant%20Box.md)
* [Option 2 - setup all the dependancies needed](https://bitbucket.org/atlassianlabs/atlassian-connect-webinar/wiki/Installing%20prerequisites%20on%20your%20machine.md) 

## Labs

* [Lab 1](https://bitbucket.org/atlassianlabs/atlassian-connect-webinar/src/master/instructions/exercise1/instructions.md)
* [Lab 2](https://bitbucket.org/atlassianlabs/atlassian-connect-webinar/src/master/instructions/exercise2/instructions.md)
* [Lab 3](https://bitbucket.org/atlassianlabs/atlassian-connect-webinar/src/master/instructions/exercise3/instructions.md)
* [Lab 4](https://bitbucket.org/atlassianlabs/atlassian-connect-webinar/src/master/instructions/exercise4/instructions.md)